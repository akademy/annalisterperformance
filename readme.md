Annalister - Performances
=========================

A front end system written in NodeJS to interface with Annalist [http://annalist.net].

This is part of the FAST project [http://www.semanticaudio.ac.uk] - Bringing the very latest technologies to the music industry, end-to-end, producer to consumer.

Currently implemented at [http://performances.annalist.net].